﻿using Common;
using Rebus.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consumer.Handlers
{
    public class MessageDataHandler : IHandleMessages<MessageData1>
    {
        private readonly Proxy _proxy;

        public async Task Handle(MessageData1 message)
        {
            //Log handler started
            Console.WriteLine("Handle Msessage data1");
            //Use Proxy to send a new message
            if (message.Id % 2 == 0)
            {
                _proxy.SendMessage(message.Id);
            }

            //Lag handler completed
            Console.WriteLine("Handle Msessage data1 success");
        }

        public MessageDataHandler()
        {
            _proxy = new Proxy();
        }
    }
}
