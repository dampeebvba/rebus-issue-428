﻿using Rebus.Activation;
using Rebus.Config;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace Producer
{
    class Program
    {
        static void Main(string[] args)
        {
            var activator = new BuiltinHandlerActivator();

            Configure.With(activator)
                .Transport(t => t.UseAzureServiceBus(Constants.ConnectionString, Constants.Queue))
                .Start();

            var cont = true;
            int id = 0;
            while (cont)
            {
                id++;
                var input = Console.ReadLine();
                if (input == "exit")
                {
                    cont = false;
                }
                else
                {
                    activator.Bus.SendLocal(new MessageData1() { Id = id, Message = $"test {id}"  });
                }
            }


            activator.Dispose();

        }
    }
}
