﻿using Rebus.Activation;
using Rebus.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Proxy
    {
        public void SendMessage(int originalMessageId)
        {
            var activator = new BuiltinHandlerActivator();

            Configure.With(activator)
                .Transport(t => t.UseAzureServiceBus(Constants.ConnectionString,Constants.Queue))
                .Start();

            activator.Bus.SendLocal(new MessageData2() { Id = originalMessageId, Message = $"proxy {originalMessageId}" });

            activator.Dispose();
        }
    }
}
