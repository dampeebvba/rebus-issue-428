﻿using Autofac;
using Consumer.Handlers;
using Rebus.Activation;
using Rebus.Config;
using Rebus.Handlers;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var builder = new ContainerBuilder();
            //builder.RegisterType<MessageDataHandler>();
            //builder.RegisterType<ProxyMessageDataHandler>();µ
            builder.RegisterAssemblyTypes(assemblies)
                 .AssignableTo<IHandleMessages>()
                 .AsImplementedInterfaces();

            var container = builder.Build();

            var activator = new Rebus.Autofac.AutofacContainerAdapter(container);

            Configure.With(activator)
                .Transport(t => t.UseAzureServiceBus(Constants.ConnectionString, Constants.Queue))
                .Start();


            Console.ReadLine();
            container.Dispose();
        }
    }
}
